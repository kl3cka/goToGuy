var express = require('express'),
    app = express(),
    developers = require('../data/developers'),
    exphbs = require('express-handlebars');

app.engine('.hbs', exphbs({defaultLayout: 'single', layoutsDir: __dirname + '/templates/layouts', partialsDir: __dirname + '/templates/partials/', extname: '.hbs'}));
app.set('view engine', '.hbs');
app.set('views', __dirname + '/templates');
app.use(express.static(__dirname + '/public'));

app.get('/', function (req, res) {
    var name = req.query.name.toUpperCase().replace(/\s+/g, ''),
        developersCopy = developers.slice(),
        randomInt;

    developersCopy.splice(developersCopy.indexOf(name), 1);
    randomInt = getRandomInt(0, developersCopy.length - 1);
    res.send(developersCopy[randomInt]);
});

app.get('/index.htm', function (req, res) {
    res.render('index', {title: "GoToGuy", developers: developers});
});

app.listen(8100, function () {
    console.log('App listening on port 8100!');
});

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

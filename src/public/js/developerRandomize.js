(function () {
    'use strict';
    window.onload = function () {
        document.getElementById('js_developersForm').addEventListener('submit', function (event) {
            document.getElementById('js_newGoToGuy').innerHTML = determineNewGoToGuy();
            event.preventDefault();
        });
    };

    function determineNewGoToGuy() {
        var developersArray = getNotSelectedDevelopers(),
            randomInt = getRandomInt(0, developersArray.length -1);

        return developersArray[randomInt];
    }

    function getNotSelectedDevelopers() {
        var developerOptionElements = document.getElementById('js_developersSelect').children,
            developersArray = [],
            i;

        for (i = 0; i < developerOptionElements.length; i++) {
            if (!developerOptionElements[i].selected) {
                developersArray.push(developerOptionElements[i].value);
            }
        }

        return developersArray;
    }

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
})();